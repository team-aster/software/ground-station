# Ground Station

This is the repository for the ASTER ground station.
It was successfully used during the flight on a REXUS rocket on the 21st March 2023.

## Installation

Clone this repository with all its submodules:
```shell
git clone --recurse-submodules https://gitlab.com/team-aster/software/ground-station.git
```

### Docker configuration

On Windows, edit the following file:
`%APPDATA%\Docker\settings.json`

Find the `vpnKitMaxPortIdleTime` setting and change it to `0`. Then restart the docker service.

### Update the installation

To keep the local copy of the groundstation up to date to changes on the git repository, run these two commands:
```shell
git pull --recurse-submodules
```

## Start the ground station

To start the ground station, the following steps need to be performed.

* Make sure the docker daemon is running (e.g. by starting Docker Desktop on Windows).
* If the experiment is connected via a serial port, start the
  [serialToDockerProxy script](Scripts/serialToDockerProxy.py). The script requires as an argument
  the serial port, to which the experiment is connected to:
  ```shell
  serialToDockerProxy.py --serial COM8
  ```
* Otherwise, set the ip address and the port of the experiment
  in the [configuration file](docker-files/config.ini).
* Run the following command in the [top directory of this repository](.)
  (e.g. the directory where this readme file is in).
  ```shell
  docker-compose up -d
  ```

The Grafana interface should be accessible at http://localhost:3000 after 10 to 20 seconds.

## Stop the ground station

To stop the ground station, terminate the
[serialToDockerProxy script](Scripts/serialToDockerProxy.py), if it was started, and execute the
following command in the [top directory of this repository](.):
```shell
docker-compose down
```

# Editing the Grafana dashboard
The Grafana dashboard can be changed using the web interface of Grafana.
After the changes have been made and the dashboard was saved, the new dashboard has to be copied
into the git repository by running the [syncGrafanaDashboards.py](Scripts/syncGrafanaDashboards.py)
script:
```shell
syncGrafanaDashboards.py
```
