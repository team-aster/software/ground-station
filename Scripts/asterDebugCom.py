#! /usr/bin/env python3
# -*- coding: utf-8 -*-
""" Provides a debugging interface to view telemetry and send telecommands. """

import os
import socket
import sys
import json

from abc import ABC, abstractmethod
from enum import Enum
from select import select
from datetime import datetime
from argparse import ArgumentParser
from threading import Event
from collections import deque

from PySide6.QtCore import Signal, Qt, QThread, QSortFilterProxyModel, QModelIndex, QAbstractTableModel
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout, QHBoxLayout, QCheckBox, QPushButton, \
    QTableView, QHeaderView, QComboBox, QGroupBox, QToolButton, QTextEdit, QDialog, QLineEdit, QFileDialog, \
    QListWidget, QAbstractItemView
from serial import Serial
from serial.tools.list_ports import comports

try:
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), '..', 'docker-files', 'communication', 'ECom')))

    from ecom.message import iterateRequiredDatapoints
    from ecom.checksum import ChecksumVerifier
    from ecom.database import CommunicationDatabase
    from ecom.response import ResponseTelemetryParser
    from ecom.datatypes import ArrayType, StructType, DynamicSizeError, loadTypedValue
    from ecom.serializer import DependantTelecommandDatapointType
    DEFAULT_DATA_DIR = os.path.abspath(os.path.join(
        os.path.dirname(__file__), '..', 'docker-files', 'communication'))
except ImportError:
    raise ImportError('Shared communication database module missing, '
                      'run "git submodule update --init"')


class AsterConnection(ABC):
    """ A connection to the Aster experiment. """
    @abstractmethod
    def open(self):
        """ Open the connection. """

    @abstractmethod
    def close(self):
        """ Close the connection. """

    @abstractmethod
    def isOpen(self) -> bool:
        """
        :return: Whether the connection is open.
        """

    @abstractmethod
    def read(self) -> bytes:
        """
        Read some bytes from the connection.

        :return: The bytes that have been read.
        """

    @abstractmethod
    def write(self, buffer: bytes):
        """
        Send the data to the experiment.

        :param buffer: The data to send.
        """


class SocketAsterConnection(AsterConnection):
    """ A connection to the Aster experiment via a socket. """

    def __init__(self, host: str, port: int):
        super().__init__()
        self._host = host
        self._port = port
        self._socket = None

    def open(self):
        self._socket = socket.create_connection((self._host, self._port))
        self._socket.setblocking(False)

    def close(self):
        currentSocket = self._socket
        if currentSocket is not None:
            currentSocket.close()
            self._socket = None

    def isOpen(self) -> bool:
        currentSocket = self._socket
        if currentSocket is None:
            return False
        if getattr(currentSocket, '_closed'):
            self._socket = None
            return False
        return True

    def read(self) -> bytes:
        currenSocket = self._socket
        if currenSocket is None:
            raise socket.error('The connection is not established')
        while True:
            readyToRead, _, _ = select([currenSocket], [], [], 5)
            if not readyToRead:
                continue
            receiveBuffer = currenSocket.recv(8192)
            if receiveBuffer:
                return receiveBuffer
            self.close()
            raise socket.error('The connection was closed')

    def write(self, buffer: bytes):
        currenSocket = self._socket
        if currenSocket is None:
            raise socket.error('The connection is not established')
        try:
            currenSocket.sendall(buffer)
        except socket.error:
            self.close()
            raise


class SerialAsterConnection(AsterConnection):
    """ A connection to the Aster experiment via a serial connection. """
    def __init__(self, port: str, baudrate: int):
        """
        Initialize the serial ASTER connection.

        :param port: The serial port on the computer where the experiment is connected trough.
        :param baudrate: The serial baudrate used for the connection.
        """
        super().__init__()
        self._serial = Serial(baudrate=baudrate, timeout=1)
        try:
            self._serial.set_buffer_size(rx_size=640000)
        except AttributeError:
            pass  # Not all systems support setting the buffer size
        self._serial.setPort(port)

    def open(self):
        self._serial.open()

    def close(self):
        self._serial.close()

    def read(self) -> bytes:
        return self._serial.read(self._serial.inWaiting() or 1)

    def write(self, buffer: bytes):
        self._serial.write(buffer)

    def isOpen(self) -> bool:
        return self._serial.isOpen()

    def setPort(self, port):
        """
        Set the port to the experiment.

        :param port: The new port.
        """
        self._serial.setPort(port)


class AsterConnectionThread(QThread):
    """ A thread responsible for handling data transfer between the experiment and the ground station. """
    onTelemetry = Signal(datetime, list)
    onError = Signal(Exception)
    onDisconnect = Signal()

    def __init__(self, connection: AsterConnection, parser):
        """
        Initialize the connection thread.

        :param connection: The connection to the Aster experiment.
        :param parser: A telemetry parser to parse incoming telemetry from the experiment.
        """
        super().__init__()
        self._parser = parser
        self._connection = connection
        self._connectedEvent = Event()
        self._isConnected = False
        self._running = True
        self._recordingFile = None

    @property
    def isConnected(self):
        """
        :return: Whether the thread is currently connected to the experiment.
        """
        return self._isConnected

    @property
    def isRecording(self):
        """
        :return: Whether the thread is currently recording telemetry.
        """
        return self._recordingFile is not None

    def startRecording(self, path):
        """
        Start recording telemetry.

        :param path: The path to the file that the recording should be saved to.
        """
        self._recordingFile = open(path, 'wb')

    def stopRecording(self):
        """ Stop recording telemetry. """
        self._recordingFile = None

    def run(self):
        """ Handle telemetry from the experiment and send them to the main thread. """
        try:
            self.connectToExperiment()
        except IOError:
            self.onDisconnect.emit()
        currentRecordingFile = self._recordingFile
        try:
            while self._running:
                if currentRecordingFile != self._recordingFile:
                    if currentRecordingFile is not None:
                        currentRecordingFile.close()
                    currentRecordingFile = self._recordingFile
                if not self._connection.isOpen():  # Check if we are still connected
                    if self._isConnected:
                        self.onDisconnect.emit()
                    self._isConnected = False
                    # Wait until we establish a new connection or 1 second passes
                    if not self._connectedEvent.wait(1):
                        continue
                    self._connectedEvent.clear()
                try:
                    buffer = self._connection.read()
                except IOError as error:
                    self.onError.emit(error)
                    self._connection.close()
                    continue
                if buffer:
                    telemetries = list(self._parser.parse(buffer, errorHandler=self._onParserError))
                    self.onTelemetry.emit(datetime.now(), telemetries)
                    if currentRecordingFile is not None:
                        currentRecordingFile.write(buffer)
        finally:
            if currentRecordingFile is not None:
                currentRecordingFile.close()

    def connectToExperiment(self):
        """ Try to establish a connection to the experiment. """
        self._connection.open()
        self._isConnected = True
        self._connectedEvent.set()  # Notify the reader thread.

    def stop(self):
        """ Stop the read thread. """
        self._running = False

    def _onParserError(self, error):
        """
        Handle an error that occurred during parsing.

        :param error: The parsing error.
        """
        self.onError.emit(error)


class TelecommandParameterContainer(QWidget):
    """ A container widget for telecommand parameters. """
    onParameterChange = Signal()  # Signal that is emitted when the parameter value changes.

    def __init__(self, parameter, containers, *args):
        """
        Initialize the container.

        :param parameter: The containers telecommand parameter.
        :param args: Additional args for the container.
        """
        super().__init__(*args)
        self._parameter = parameter
        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(4, 4, 4, 4)
        self.setVisible(False)
        self.layout.addWidget(QLabel(f'{parameter.name}: '))
        self._valueWidget = None
        if isinstance(parameter, DependantTelecommandDatapointType):
            for container in containers:
                if container.getParameter() == self._parameter.provider:
                    break
            else:
                raise RuntimeError('Unable to find provider parameter container')
            self._providerContainer = container
            container.onParameterChange.connect(self._onProviderParameterChanged)
            try:
                parameter = parameter.configureWith(container.getParameterValue())
            except ValueError:
                return
        self._configureForParameter(parameter)

    def show(self):
        if self._valueWidget:
            super().show()

    def setVisible(self, visible: bool):
        if visible and self._valueWidget is None:
            return
        super().setVisible(visible)

    def getParameter(self):
        """
        :return: The parameter that this container is associated with.
        """
        return self._parameter

    def getParameterValue(self):
        """
        :return: The value of the parameter from the widget.
        """
        if self._valueWidget is None:
            raise ValueError('setValueWidget was not called')
        if isinstance(self._valueWidget, QComboBox):
            return self._valueWidget.currentData()
        elif isinstance(self._valueWidget, QCheckBox):
            return self._valueWidget.isChecked()
        elif isinstance(self._parameter, DependantTelecommandDatapointType):
            parameter = self._parameter.configureWith(self._providerContainer.getParameterValue())
        else:
            parameter = self._parameter
        return loadTypedValue(self._valueWidget.text(), parameter.type.type)

    def _onProviderParameterChanged(self):
        """ Handle a change to the selected value of a parameter that this parameter depends on. """
        parameter = self._parameter.configureWith(self._providerContainer.getParameterValue())
        self._configureForParameter(parameter)

    def _configureForParameter(self, parameter):
        """
        Configure the visual representation of this parameter container based on the parameter.

        :param parameter: The parameter to configure for.
        """
        self.setToolTip(parameter.description)
        if issubclass(parameter.type.type, Enum):
            widget = QComboBox()
            widget.currentIndexChanged.connect(lambda *args: self.onParameterChange.emit())
            for value in parameter.type.type:
                widget.addItem(value.name, value)
            if parameter.default is not None:
                widget.setCurrentText(parameter.default.name)
        elif issubclass(parameter.type.type, bool):
            widget = QCheckBox()
            widget.setChecked(False if parameter.default is None else parameter.default)
            widget.stateChanged.connect(lambda *args: self.onParameterChange.emit())
        else:
            if parameter.default is None:
                defaultValue = ''
            elif isinstance(parameter.default, bytes):
                defaultValue = parameter.default.decode('utf-8')
            elif isinstance(parameter.default, (dict, list)):
                defaultValue = json.dumps(parameter.default)
            else:
                defaultValue = str(parameter.default)
            widget = QLineEdit(defaultValue)
            widget.setFixedWidth(120)
            widget.editingFinished.connect(lambda *args: self.onParameterChange.emit())
        if self._valueWidget:
            self.layout.removeWidget(self._valueWidget)
        self._valueWidget = widget
        self.layout.addWidget(widget)


class TelemetryModel(QAbstractTableModel):
    """ A model for the telemetry data. """

    def __init__(self, *args):
        super().__init__(*args)
        self._MAX_LENGTH = 2048
        self._telemetry = deque(maxlen=self._MAX_LENGTH + 1)
        self._headers = ['Time', 'Type', 'Level', 'Content']
        self._HEADER_LENGTH = len(self._headers)

    def rowCount(self, parent=None):
        return len(self._telemetry)

    def columnCount(self, parent=None):
        return self._HEADER_LENGTH

    def headerData(self, section, orientation, role=None):
        if role == Qt.DisplayRole:
            return self._headers[section]

    def data(self, index, role=None):
        if not index.isValid():
            return None
        if role == Qt.DisplayRole or role == Qt.EditRole:
            row = self._telemetry[index.row()]
            if index.column() == 0:
                return row[0]
            elif index.column() == 1:
                return row[1].name
            elif index.column() == 2:
                return row[2].name.replace('_MESSAGE', '', 1).replace('_ERROR', '', 1)
            else:
                return row[3].replace('\n', ', ')
        elif role == Qt.UserRole:
            return self._telemetry[index.row()][index.column()]

    def addTelemetry(self, time, typ, level, data):
        """
        Get the formatted items for the telemetry table.

        :param time: The time of the telemetry.
        :param typ: The type of the telemetry.
        :param level: The level of the telemetry.
        :param data: The data of the telemetry
        """
        position = len(self._telemetry)
        self.beginInsertRows(QModelIndex(), position, position)
        self._telemetry.append((time.strftime('%H:%M:%S.%f')[:-3], typ, level, data))
        self.endInsertRows()
        if position >= self._MAX_LENGTH:
            self.removeRows(0, 1)

    def insertRows(self, position, rows, parent=QModelIndex()):
        self.beginInsertRows(parent, position, position + rows - 1)
        for row in range(rows):
            self._telemetry.insert(position + row, [None] * self._HEADER_LENGTH)
        self.endInsertRows()
        return True

    def clear(self):
        """ Clear all telemetry from the model. """
        self.beginRemoveRows(QModelIndex(), 0, len(self._telemetry) - 1)
        self._telemetry.clear()
        self.endRemoveRows()

    def removeRows(self, position, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, position, position + rows - 1)
        for row in range(rows):
            del self._telemetry[position]
        self.endRemoveRows()
        return True


class TelemetryFilterModel(QSortFilterProxyModel):
    """ A model that filters the telemetry. """

    def __init__(self, levelFilter=None, typeFilter=None, *args):
        """
        Initialize the filter model.

        :param levelFilter: The initial telemetry level filter.
        :param typeFilter: The initial telemetry type filter.
        :param args: Additional arguments for the model.
        """
        super().__init__(*args)
        self._levelFilter = set() if levelFilter is None else levelFilter
        self._typeFilter = set() if typeFilter is None else typeFilter

    def setLevelFilter(self, filterType, allow):
        """
        Allow or filter a certain telemetry level.

        :param filterType: The telemetry level to change.
        :param allow: Whether to allow or filter the level.
        """
        if allow:
            self._levelFilter.add(filterType)
        else:
            self._levelFilter.remove(filterType)
        self.invalidateFilter()

    def setTypeFilter(self, filterType, allow):
        """
        Allow or filter a certain telemetry type.

        :param filterType: The telemetry type to change.
        :param allow: Whether to allow or filter the type.
        """
        if allow:
            self._typeFilter.add(filterType)
        else:
            self._typeFilter.remove(filterType)
        self.invalidateFilter()

    def isLevelAllowed(self, level):
        """
        :param level: A telemetry level to check.
        :return: Whether the telemetry level is allowed.
        """
        return level in self._levelFilter

    def isTypeAllowed(self, typ):
        """
        :param typ: A telemetry type to check.
        :return: Whether a telemetry type is allowed.
        """
        return typ in self._typeFilter

    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        :param sourceRow: The index of the row.
        :param sourceParent: The parent of the row.
        :return: Whether the row is allowed or filtered.
        """
        sourceModel = self.sourceModel()
        typ = sourceModel.data(sourceModel.index(sourceRow, 1, sourceParent), Qt.UserRole)
        level = sourceModel.data(sourceModel.index(sourceRow, 2, sourceParent), Qt.UserRole)
        return self.isTypeAllowed(typ) and self.isLevelAllowed(level)


class AsterDebugUi(QApplication):
    """ The user interface of the debugging interface for ASTER. """

    def __init__(self, arguments):
        """
        Initialize the user interface.

        :param arguments: Command line arguments.
        """
        super().__init__()
        self._database = CommunicationDatabase(DEFAULT_DATA_DIR)
        self._parser = ResponseTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))
        self._lastHeartBeat = 0
        # Prepare the different level and telemetry types for easier comparison later.
        self.TELEMETRY_MESSAGE = self._database.getTelemetryByName('MESSAGE').id
        self.TELEMETRY_ACKNOWLEDGE = self._database.getTelemetryByName('ACKNOWLEDGE').id
        self.TELEMETRY_DATA = self._database.getTelemetryByName('DATA').id
        self.TELEMETRY_CAMERA_DATA = self._database.getTelemetryByName('CAMERA_DATA').id
        self.TELEMETRY_HEARTBEAT = self._database.getTelemetryByName('HEARTBEAT').id
        self.TELEMETRY_RESPONSE = self._database.getTelemetryByName('RESPONSE').id
        self.LEVEL_ENUM = self._database.dataTypes['MessageType'].type
        self.INFO_LEVEL = self.LEVEL_ENUM['INFO_MESSAGE']
        self._telemetryModel = TelemetryModel()

        # Generate the main layout of the user interface.
        EXPAND = 1
        self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), 'aster.ico')))
        self._mainWindow = QWidget()
        self._mainWindow.resize(1200, 800)
        self._mainWindow.setWindowTitle('ASTER - Debug Interface')
        mainLayout = self._mainWindow.layout = QVBoxLayout(self._mainWindow)

        topBar = QWidget()
        topBar.layout = QHBoxLayout(topBar)
        topBar.layout.setContentsMargins(0, 0, 0, 0)
        topBar.layout.addWidget(QLabel('Telemetry:'))
        topBar.layout.addStretch()
        self._processorTime = QLabel()
        self._processorTime.setContentsMargins(0, 0, 4, 0)
        topBar.layout.addWidget(self._processorTime)
        self._autoScrollCheckbox = QCheckBox('Auto scroll')
        self._autoScrollCheckbox.setChecked(True)
        self._autoScrollCheckbox.stateChanged.connect(self._onAutoScrollChanged)
        topBar.layout.addWidget(self._autoScrollCheckbox)
        self._recordButton = QPushButton()
        self._recordButton.setText('Record')
        self._recordButton.released.connect(self._onRecordButtonClicked)
        topBar.layout.addWidget(self._recordButton)
        self._filterButton = QPushButton()
        self._filterButton.setText('Filter')
        self._filterButton.released.connect(self._onFilterButtonClicked)
        topBar.layout.addWidget(self._filterButton)
        clearButton = QPushButton('Clear')
        clearButton.released.connect(self._telemetryModel.clear)
        topBar.layout.addWidget(clearButton)
        mainLayout.addWidget(topBar)

        self._telemetryFilterModel = TelemetryFilterModel(set(self.LEVEL_ENUM), {
            self.TELEMETRY_MESSAGE, self.TELEMETRY_DATA, self.TELEMETRY_CAMERA_DATA, self.TELEMETRY_RESPONSE,
            self.TELEMETRY_HEARTBEAT})
        self._telemetryFilterModel.setSourceModel(self._telemetryModel)
        self._telemetryView = QTableView()
        self._telemetryView.setModel(self._telemetryFilterModel)
        self._telemetryView.verticalHeader().hide()
        self._telemetryView.horizontalHeader().setSectionResizeMode(3, QHeaderView.Stretch)
        self._telemetryView.setColumnWidth(0, 75)
        self._telemetryView.setColumnWidth(2, 70)
        self._telemetryView.verticalScrollBar().valueChanged.connect(self._onTelemetryScroll)
        self._telemetryView.doubleClicked.connect(self._onTelemetryDoubleClicked)
        mainLayout.addWidget(self._telemetryView)

        telecommandRow = QWidget()
        telecommandRow.layout = QHBoxLayout(telecommandRow)
        telecommandRow.layout.setContentsMargins(0, 0, 0, 0)
        telecommandSelectionColumn = QWidget()
        telecommandSelectionColumn.layout = QVBoxLayout(telecommandSelectionColumn)
        telecommandSelectionColumn.layout.setContentsMargins(0, 4, 0, 4)
        telecommandSelectionColumn.layout.addWidget(QLabel('Telecommand:'))
        self._telecommandParametersBox = QGroupBox('Parameters')
        self._telecommandParametersBox.layout = QHBoxLayout(self._telecommandParametersBox)
        self._telecommandParametersBox.layout.setContentsMargins(0, 0, 0, 0)
        self._generateParametersUi()
        self._telecommandSelector = QComboBox()
        self._telecommandSelector.currentIndexChanged.connect(self._onTelecommandSelectionChanged)
        for telecommand in self._database.telecommandTypes:
            self._telecommandSelector.addItem(telecommand.id.name, telecommand)
        telecommandSelectionColumn.layout.addWidget(self._telecommandSelector)
        telecommandRow.layout.addWidget(telecommandSelectionColumn)
        telecommandRow.layout.addStretch()
        telecommandRow.layout.addWidget(self._telecommandParametersBox, EXPAND)
        mainLayout.addWidget(telecommandRow)

        actionBar = QWidget()
        actionBar.layout = QHBoxLayout(actionBar)
        actionBar.layout.setContentsMargins(0, 8, 0, 0)
        self._comPortSelectButton = QPushButton('Select port')
        self._comPortSelectButton.released.connect(self._onSelectComPort)
        self._comPortSelectButton.setVisible(False)
        actionBar.layout.addWidget(self._comPortSelectButton)
        self._actionButton = QPushButton('Send')
        self._actionButton.released.connect(self._onActionButtonClicked)
        actionBar.layout.addWidget(self._actionButton)
        self._statusHistoryButton = QToolButton()
        self._statusHistoryButton.setStyleSheet("QToolButton { border: none; width: 14px;}")
        self._statusHistoryButton.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonTextBesideIcon)
        self._statusHistoryButton.setArrowType(Qt.ArrowType.RightArrow)
        self._statusHistoryButton.setCheckable(True)
        self._statusHistoryButton.setChecked(False)
        self._statusHistoryButton.toggled.connect(self._onExpandStatusHistoryClicked)
        actionBar.layout.addWidget(self._statusHistoryButton)
        self._statusLabel = QLabel('Ready')
        self._statusLabel.setStyleSheet('QLabel { color: green; }')
        actionBar.layout.addWidget(self._statusLabel, EXPAND)
        mainLayout.addWidget(actionBar)

        self._statusHistory = QTextEdit()
        self._statusHistory.setVisible(False)
        mainLayout.addWidget(self._statusHistory)

        # Determine the connection settings
        host = arguments.host
        if host is None:
            serialPort = arguments.serial
            if serialPort is None:
                allPorts = comports()
                if len(allPorts) == 1:
                    serialPort = allPorts[0].device
                else:
                    serialPort = self._selectComPort()
            self._connection = SerialAsterConnection(serialPort, arguments.baudrate)
        else:
            self._connection = SocketAsterConnection(host, arguments.port)
            self._comPortSelectButton.hide()

        # Create and start the connection thread
        self._connectionThread = AsterConnectionThread(self._connection, self._parser)
        self._connectionThread.onTelemetry.connect(self._onNewTelemetry)
        self._connectionThread.onDisconnect.connect(self._onDisconnected)
        self._connectionThread.onError.connect(
            lambda error: self._setStatus(f'Communication error: {error}', color='red'))
        self._connectionThread.start()

        self.aboutToQuit.connect(self._onQuit)
        self._mainWindow.show()

    def _onFilterButtonClicked(self):
        """ Show the filter menu popup. """
        filterPopup = QDialog()
        filterPopup.setWindowFlags(Qt.Popup)
        filterPopup.setWindowTitle('Filter')
        filterPopup.layout = QVBoxLayout(filterPopup)
        typeContainer = QGroupBox('Type')
        typeContainer.layout = QVBoxLayout(typeContainer)
        for typ in self._database.telemetryTypeEnum:
            checkBox = QCheckBox(typ.name)
            checkBox.setChecked(self._telemetryFilterModel.isTypeAllowed(typ))
            checkBox.stateChanged.connect(
                lambda state, filterElement=typ: self._telemetryFilterModel.setTypeFilter(
                    filterElement, Qt.CheckState(state) is Qt.Checked))
            typeContainer.layout.addWidget(checkBox)
        filterPopup.layout.addWidget(typeContainer)
        levelContainer = QGroupBox('Level')
        levelContainer.layout = QVBoxLayout(levelContainer)
        for level in self._database.dataTypes['MessageType'].type:
            checkBox = QCheckBox(level.name.replace('_MESSAGE', '', 1).replace('_ERROR', '', 1))
            checkBox.setChecked(self._telemetryFilterModel.isLevelAllowed(level))
            checkBox.stateChanged.connect(
                lambda state, filterElement=level: self._telemetryFilterModel.setLevelFilter(
                    filterElement, Qt.CheckState(state) is Qt.Checked))
            levelContainer.layout.addWidget(checkBox)
        filterPopup.layout.addWidget(levelContainer)

        filterButtonGeometry = self._filterButton.geometry()
        position = self._filterButton.parentWidget().mapToGlobal(filterButtonGeometry.bottomLeft())
        filterPopup.updateGeometry()
        filterPopup.adjustSize()
        position.setX(position.x() + filterButtonGeometry.width() / 2 - filterPopup.width() / 2)
        filterPopup.move(position)
        filterPopup.show()
        filterPopup.setFocus()
        filterPopup.exec()

    def _onRecordButtonClicked(self):
        """ Start or stop the recording of telemetry. """
        if self._connectionThread.isRecording:
            self._connectionThread.stopRecording()
            self._recordButton.setText('Record')
        else:
            path, _ = QFileDialog.getSaveFileName(
                caption='Save recording', dir='', filter='Binary File (*.bin)')
            self._connectionThread.startRecording(path)
            self._recordButton.setText('Stop recording')

    def _onExpandStatusHistoryClicked(self, expandHistory):
        """ Expand or collapse the status history. """
        self._statusHistory.setVisible(expandHistory)
        self._statusHistoryButton.setArrowType(
            Qt.ArrowType.DownArrow if expandHistory else Qt.ArrowType.RightArrow)

    def _onTelecommandSelectionChanged(self):
        """ Update the parameters box based on the selected telecommand. """
        selectedTelecommand = self._telecommandSelector.currentData()
        self._telecommandSelector.setToolTip(selectedTelecommand.description)
        if selectedTelecommand.data:
            for child in self._telecommandParametersBox.children():
                if isinstance(child, TelecommandParameterContainer):
                    child.setVisible(child.getParameter() in selectedTelecommand.data)
            self._telecommandParametersBox.setVisible(True)
        else:
            self._telecommandParametersBox.setVisible(False)

    def _onActionButtonClicked(self):
        """ Handle a click on the action button. """
        if not self._connectionThread.isConnected:  # Execute the "Connect" action.
            try:
                self._connectionThread.connectToExperiment()
                self._setStatus('Connected', color='green')
                self._actionButton.setText('Send')
                self._comPortSelectButton.setVisible(False)
            except IOError as error:
                self._setStatus(f'Failed to connect: {error}', color='red')
        else:  # Execute the "Send" action.
            selectedTelecommand = self._telecommandSelector.currentData()
            try:
                arguments = self._getTelecommandArguments(selectedTelecommand)
            except ValueError as error:
                self._setStatus(str(error), color='orange')
            else:
                try:
                    self._connection.write(self._parser.serialize(selectedTelecommand, **arguments))
                except IOError as error:
                    self._setStatus(f'Failed to send: {error}', color='red')
                else:
                    self._setStatus('Waiting for response...', color='black', addToHistory=False)

    def _onSelectComPort(self):
        """ Handle a click on the select port button. """
        newPort = self._selectComPort()
        if newPort is not None:
            self._connection.setPort(newPort)

    def _onNewTelemetry(self, receiveTime, telemetries):
        """
        Parse the new telemetry according to the type and add it to the model.

        :param receiveTime: The time when the telemetry was received.
        :param telemetries: A list of new telemetry objects to add.
        """
        for telemetry in telemetries:
            if telemetry.type is self.TELEMETRY_MESSAGE:
                level = telemetry.data.get('type', self.INFO_LEVEL)
                data = 'time: ' + str(telemetry.data.get('time', '?')) + '\n' + \
                       telemetry.data.get('message', b'?').decode('utf-8', errors='replace')
            elif telemetry.type is self.TELEMETRY_ACKNOWLEDGE or \
                    telemetry.type is self.TELEMETRY_RESPONSE:
                commandName = telemetry.data['command'].id.name
                self._setStatus(f'{commandName} acknowledged', color='green')
                level = self.INFO_LEVEL
                data = f'Command: {commandName}'
                if telemetry.type is self.TELEMETRY_RESPONSE:
                    data += f'\nResponse: {telemetry.data["value"]}'
            elif telemetry.type in (self.TELEMETRY_DATA, self.TELEMETRY_CAMERA_DATA, self.TELEMETRY_HEARTBEAT):
                if telemetry.type is self.TELEMETRY_HEARTBEAT:
                    processorTime = telemetry.data['time']
                    self._processorTime.setText(f'Processor time: {processorTime}')
                    if self._lastHeartBeat > processorTime:
                        self._setStatus('Chip reset detected', color='orange')
                    self._lastHeartBeat = processorTime
                level = self.INFO_LEVEL
                formattedData = {}
                for name, value in telemetry.data.items():
                    if isinstance(value, float):
                        formattedData[name] = f'{value:0.3g}'
                    else:
                        formattedData[name] = value
                data = '\n'.join(f'{name}: {value!r}' for name, value in formattedData.items())
            else:
                level = self.INFO_LEVEL
                data = repr(telemetry.data)
            self._telemetryModel.addTelemetry(receiveTime, telemetry.type, level, data)
            if self._autoScrollCheckbox.isChecked():
                self._telemetryView.scrollToBottom()

    def _onDisconnected(self):
        """ Handle a disconnect from the experiment. """
        self._setStatus('Disconnected', color='orange')
        self._processorTime.setText('')
        self._actionButton.setText('Connect')
        if isinstance(self._connection, SerialAsterConnection):
            self._comPortSelectButton.setVisible(True)

    def _onQuit(self):
        """ Stop the connection thread on exiting the application. """
        self._connectionThread.stop()
        self._connectionThread.wait(2000)

    def _onAutoScrollChanged(self, enabledState):
        """
        Scroll to the bottom of the telemetry if the autoscroll was enabled.

        :param enabledState: The checked state of the autoscroll checkbox.
        """
        if enabledState == Qt.CheckState.Checked:
            self._telemetryView.scrollToBottom()

    @staticmethod
    def _selectComPort():
        """
        Show a dialog to select a communication port.

        :return: The communication port that was selected.
        """
        selectedPort = []
        popup = QDialog()
        popup.setWindowTitle('Select COM port')
        popup.layout = QVBoxLayout(popup)
        portList = QListWidget(popup)
        allPorts = comports()
        portList.addItems(f'{port.device}: {port.description}' for port in allPorts)
        portList.setSelectionMode(QAbstractItemView.SelectionMode.SingleSelection)

        def onPortSelected(item):
            selectedPort.append(allPorts[portList.row(item)].device)
            popup.close()

        portList.itemDoubleClicked.connect(onPortSelected)
        popup.layout.addWidget(portList)
        popup.layout.addWidget(QLabel('Double click to select port.'))
        popup.adjustSize()
        popup.show()
        popup.setFocus()
        popup.exec()
        return None if len(selectedPort) != 1 else selectedPort[0]

    def _setStatus(self, status, color='white', addToHistory=True):
        """
        Set the current status in the UI.

        :param status: The status message.
        :param color: The color of the status.
        :param addToHistory: Whether to add the status to the history.
        """
        self._statusLabel.setText(status)
        self._statusLabel.setStyleSheet('QLabel { color: ' + color + '; }')
        if addToHistory:
            timeStr = datetime.now().strftime('%H:%M:%S')
            self._statusHistory.setTextColor(color)
            self._statusHistory.append(f'[{timeStr}] {status}')

    def _generateParametersUi(self):
        """ Generate the UI elements for all telecommand arguments. """
        for telecommand in self._database.telecommandTypes:
            containers = []
            for parameter in iterateRequiredDatapoints(telecommand):
                container = TelecommandParameterContainer(parameter, containers)
                containers.append(container)
                self._telecommandParametersBox.layout.addWidget(container)
        self._telecommandParametersBox.layout.addStretch(1)

    def _onTelemetryScroll(self, scrollPosition):
        """
        React to a scroll event in the telemetry widget.

        :param scrollPosition: The current scroll position.
        """
        self._autoScrollCheckbox.setChecked(
            scrollPosition == self._telemetryView.verticalScrollBar().maximum())

    def _onTelemetryDoubleClicked(self, telemetryIndex):
        """
        Show a detail view of the double-clicked telemetry.

        :param telemetryIndex: The index of the clicked telemetry element.
        """
        # The index does not take into account the filtered elements.
        data = self._telemetryView.model().data(telemetryIndex, Qt.UserRole)
        popup = QDialog()
        popup.setWindowFlags(Qt.Popup)
        popup.setWindowTitle('Telemetry')
        popup.layout = QVBoxLayout(popup)
        popup.layout.addWidget(QLabel(data))
        popup.show()
        popup.setFocus()
        popup.exec()

    def _getTelecommandArguments(self, telecommand):
        """
        Get the telecommand argument values form the current UI state.

        :param telecommand: The selected telecommand.
        :return: The telecommand arguments.
        """
        arguments = {}
        for parameter in telecommand.data:
            try:
                for child in self._telecommandParametersBox.children():
                    if isinstance(child, TelecommandParameterContainer) \
                            and child.getParameter() == parameter:
                        break
                else:
                    continue
                arguments[parameter.name] = child.getParameterValue()
            except ValueError as error:
                raise ValueError(f'Value for parameter "{parameter.name}" is invalid: {error}')
        return arguments


def main():
    """ Parse the command line arguments and show the debugging interface. """
    parser = ArgumentParser(description='Debugging interface for ASTER.')
    connectionArgs = parser.add_mutually_exclusive_group(required=False)
    connectionArgs.add_argument('-s', '--serial', default=None,
                                help='The serial port that the ASTER project is connected to.')
    connectionArgs.add_argument('--host', default=None,
                                help='The host that the ASTER project can be reached on.')
    connectionParamsArgs = parser.add_mutually_exclusive_group(required=False)
    connectionParamsArgs.add_argument('-b', '--baudrate', help='The baudrate to use for the serial connection.',
                                      type=int, default=38400)
    connectionParamsArgs.add_argument('-p', '--port', help='The port on the host to connect to.',
                                      type=int, default=50000)
    arguments = parser.parse_args()
    sys.exit(AsterDebugUi(arguments).exec())


if __name__ == '__main__':
    main()
