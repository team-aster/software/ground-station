#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import struct
import sys
from argparse import ArgumentParser
from collections import OrderedDict
from datetime import datetime

from IridiumServer.client import IridiumProxyServer, ASTER_SERVER_URL

IRIDIUM_MESSAGE_SPEC = OrderedDict([
    ('payloadMessageId', 'c'),
    ('xAcceleration', 'h'),
    ('yAcceleration', 'h'),
    ('zAcceleration', 'h'),
    ('rollMotorSpeed', 'h'),
    ('pitchMotorSpeed', 'h'),
    ('yawMotorSpeed', 'h'),
    ('payloadParity', '3s'),
    ('stopBits', 'c'),
    ('platformMessageId', 'c'),
    ('gnssTime', 'I'),
    ('latitude', 'i'),
    ('longitude', 'i'),
    ('altitude', 'i'),
    ('ffuVoltage', 'h'),
    ('rollRotation', 'h'),
    ('pitchRotation', 'h'),
    ('yawRotation', 'h'),
    ('systemState', 'B'),
    ('recoveryState', 'B'),
    ('errorCounter', 'B'),
    ('platformParity', '4s'),
])
IRIDIUM_MESSAGE_FORMAT = '<' + ''.join(formatStr for formatStr in IRIDIUM_MESSAGE_SPEC.values())


def printMessage(message):
    try:
        values = dict(zip(IRIDIUM_MESSAGE_SPEC.keys(),
                          struct.unpack(IRIDIUM_MESSAGE_FORMAT, message.data)))
    except struct.error as error:
        if message.data == b'Testing...':
            print('<Testing message>')
        else:
            print(f'[ERROR] Failed to parse message: {error}')
        return

    # Validate
    if values['payloadMessageId'] != bytes([0b11001010]):
        print(f'[ERROR]: Received message with invalid payload id: {values["payloadMessageId"]}')
        return
    if values['platformMessageId'] != bytes([0b00110101]):
        print(f'[ERROR]: Received message with invalid platform id: {values["platformMessageId"]}')
        return

    # Convert
    values['xAcceleration'] *= 10 / 0x7fff
    values['yAcceleration'] *= 10 / 0x7fff
    values['zAcceleration'] *= 10 / 0x7fff
    values['rollMotorSpeed'] *= 4200 / 0x7fff
    values['pitchMotorSpeed'] *= 4200 / 0x7fff
    values['yawMotorSpeed'] *= 4200 / 0x7fff
    values['latitude'] *= 1e-7
    values['longitude'] *= 1e-7
    values['ffuVoltage'] *= 5 / 0x7fff
    values['rollRotation'] *= 5000 / 0x7fff
    values['pitchRotation'] *= 5000 / 0x7fff
    values['yawRotation'] *= 5000 / 0x7fff
    receiveTime = datetime.utcfromtimestamp(1636806223).isoformat()

    # Display
    print(f"""Message (received at {receiveTime}, GPS time {values['gnssTime']}):
    location: GPS (lat: {values['latitude']}, long: {values['longitude']}), \
altitude: {values['altitude']}mm
              Iridium (lat: {message.latitude}, long: {message.longitude}) +- {message.accuracy}km
    acceleration: (x: {values['xAcceleration']}, y: {values['yAcceleration']}, \
z: {values['zAcceleration']}) m/(s^2)
    motorSpeed: (roll: {values['rollMotorSpeed']}, pitch: {values['pitchMotorSpeed']}, \
yaw: {values['yawMotorSpeed']}) rad/s
    rotation: (roll: {values['rollRotation']}, pitch: {values['pitchRotation']}, \
yaw: {values['yawRotation']}) rad/s
    battery: {values['ffuVoltage']}V
    state: System: {values['systemState']}, Recovery: {values['recoveryState']}
    Number of recorded error messages: {values['errorCounter']}""")


def main():
    """
    Print all Iridium messages stored on the server, then wait and print new incoming messages.

    :return: The return code of the program.
    """
    parser = ArgumentParser(description='Client for the Iridium server that will print '
                                        'all old messages and listen for new ones')
    parser.add_argument('-t', '--token', required=True,
                        help='The token to communicate with the Aster Iridium server.')
    parser.add_argument('-u', '--url', default=ASTER_SERVER_URL,
                        help='The url of the Aster Iridium server.')
    arguments = parser.parse_args()

    server = IridiumProxyServer(arguments.token, arguments.url)
    try:
        oldMessages = server.getAllExistingMessages()
        if not oldMessages:
            print('No previous Iridium messages')
        else:
            print('Previous Iridium messages:')
            for message in oldMessages:
                printMessage(message)
        print('\nListening for new messages...')
        server.listenForNewMessages(printMessage)
    except IOError as error:
        print(f'[ERROR] Failed to connect to Iridium proxy server: {error}')
        return 1
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    sys.exit(main())
