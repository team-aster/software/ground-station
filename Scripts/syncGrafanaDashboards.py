#! /usr/bin/env python3
# -*- coding: utf-8 -*-
""" Synchronize the current Grafana configurations and dashboards to the git repository. """

import os
import sys
import json

from argparse import ArgumentParser

from grafana_api import GrafanaFace
from grafana_api.grafana_api import GrafanaException


def filterTemplatingInfo(templating):
    """
    Filter current values and errors from templating variables.

    :param templating: The templating data of the dashboard.
    :return: The filtered templating data.
    """
    filteredTemplating = {}
    for name, value in templating.items():
        if not isinstance(value, list):
            filteredTemplating[name] = value
            continue
        templateVariables = []
        for templateVariable in value:
            templateVariables.append({name: value for name, value in templateVariable.items() if
                                      name not in ['error', 'current']})
        filteredTemplating[name] = templateVariables
    return filteredTemplating


def saveDashboards(grafana, grafanaDir):
    """
    Save all dashboards in the Grafana instance to the given directory.

    :param grafana: The Grafana instance.
    :param grafanaDir: A directory in which the dashboards will be saved.
    """
    dashboardsDir = os.path.join(grafanaDir, 'dashboards')
    print(f'Checking for dashboards...', end='\r', flush=True)
    dashboards = grafana.search.search_dashboards()
    if not dashboards:
        print('No dashboards found       ')
        return
    for i, dashboardInfo in enumerate(dashboards):
        print(f'Downloading dashboard ({i + 1} / {len(dashboards)})', end='\r', flush=True)
        name = dashboardInfo['title']
        dashboard = grafana.dashboard.get_dashboard(dashboardInfo['uid'])['dashboard']
        dashboard = {name: filterTemplatingInfo(value) if name == 'templating' else value
                     for name, value in dashboard.items()
                     if name not in ['id', 'iteration', 'version']}
        print(f'Saving dashboard ({i} / {len(dashboards)})', end='\r', flush=True)
        with open(os.path.join(dashboardsDir, f'{name}.json'), 'w') as dashboardFile:
            json.dump(dashboard, dashboardFile, indent=2)
    print('Saving dashboards   DONE' + ' ' * 16)


def main():
    """
    Synchronize the dashboards from Grafana to the Grafana folder in the Git repository.

    :return: Whether the operation was successfully.
    """
    parser = ArgumentParser(description=__doc__.strip())
    parser.add_argument('-u', '--username', default='admin',
                        help='The username to use when communicating with Grafana.')
    parser.add_argument('-p', '--password', default='admin',
                        help='The username to use when communicating with Grafana.')
    parser.add_argument('-a', '--address', default='localhost:3000',
                        help='The network address of Grafana.')
    arguments = parser.parse_args()

    try:
        grafana = GrafanaFace(auth=(arguments.username, arguments.password), host=arguments.address)
    except GrafanaException as error:
        print(f'[ERROR] Failed to connect to Grafana: {error}')
        return 1
    grafanaDir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'Grafana'))
    try:
        saveDashboards(grafana, grafanaDir)
    except (GrafanaException, IOError) as error:
        print(f'[ERROR] Failed to save the dashboards: {error}')
        return 1


if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        pass
