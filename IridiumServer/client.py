#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from typing import List, Callable, Dict, Optional
from argparse import ArgumentParser
from dataclasses import dataclass

import socketio
import requests

ASTER_SERVER_URL = 'https://aster.project-faster.com:8443'


@dataclass(frozen=True)
class IridiumMessage:
    time: int  # Unix timestamp when the message was received.
    data: bytes  # The contents of the message.
    latitude: float  # The latitude measured by the Iridium network where the message was sent.
    longitude: float  # The longitude measured by the Iridium network where the message was sent.
    accuracy: int  # The accuracy of the Iridium location measurement in kilometers.

    @staticmethod
    def fromJson(data: Dict) -> 'IridiumMessage':
        return IridiumMessage(
            time=data['time'],
            data=bytes.fromhex(data['data']),
            latitude=data['latitude'],
            longitude=data['longitude'],
            accuracy=data['accuracy'],
        )


class IridiumProxyServer:
    """ Represents a proxy server that stores messages delivered via the Iridium network. """

    def __init__(self, token: str, url: str = ASTER_SERVER_URL):
        """
        Initialize a reference to the Iridium proxy server.

        :param token: The token to use for requests to the proxy server.
        :param url: The url of the proxy server.
        """
        super().__init__()
        self._token = 'Bearer ' + token
        self._url = url

    def getAllExistingMessages(
            self, deserializeErrorHandler: Optional[Callable[[Dict, Exception], None]] = None) -> List[IridiumMessage]:
        """
        Request all existing messages from the server.

        :param deserializeErrorHandler: An error handler that will be called with the data and error
                                        if a message was malformed.
        :return: The list of Iridium messages on the server.
        """
        request = requests.get(self._url, headers={'Authorization': self._token})
        request.raise_for_status()
        messages = []
        for data in request.json():
            try:
                messages.append(IridiumMessage.fromJson(data))
            except (ValueError, KeyError) as error:
                if deserializeErrorHandler is None:
                    raise error
                deserializeErrorHandler(data, error)
        return messages

    def listenForNewMessages(self, handler: Callable[[IridiumMessage], None]) -> None:
        """
        Block and listen for new Iridium messages that arrive at the server.

        :param handler: A handler that will be called if a new message was received.
        """
        sio = socketio.Client()
        sio.on('message', lambda data: handler(IridiumMessage.fromJson(data)))
        sio.connect(self._url, headers={'Authorization': self._token})


def main():
    """
    Print all Iridium messages stored on the server, then wait and print new incoming messages.

    :return: The return code of the program.
    """
    parser = ArgumentParser(description='Client for the Iridium server that will print '
                                        'all old messages and listen for new ones')
    parser.add_argument('-t', '--token', required=True,
                        help='The token to communicate with the Aster Iridium server.')
    parser.add_argument('-u', '--url', default=ASTER_SERVER_URL,
                        help='The url of the Aster Iridium server.')
    arguments = parser.parse_args()

    server = IridiumProxyServer(arguments.token, arguments.url)
    try:
        oldMessages = server.getAllExistingMessages()
        if not oldMessages:
            print('No previous Iridium messages')
        else:
            print('Previous Iridium messages:')
            for message in oldMessages:
                print(message)
        print('\nListening for new messages...')
        server.listenForNewMessages(lambda data: print(data))
    except IOError as error:
        print(f'[ERROR] Failed to connect to Iridium proxy server: {error}')
        return 1
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    sys.exit(main())
