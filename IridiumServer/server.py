#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import sqlite3

from time import strptime
from calendar import timegm
from argparse import ArgumentParser
from contextlib import closing
from collections import OrderedDict

from flask import Flask, request, jsonify, g
from flask_httpauth import HTTPTokenAuth
from flask_socketio import SocketIO, ConnectionRefusedError

# noinspection SpellCheckingInspection
DATABASE_SCHEMA = OrderedDict([
    ('id', (int, 'momsn')),
    ('time', (lambda value: timegm(strptime('20' + value, '%Y-%m-%d %H:%M:%S')), 'transmit_time')),
    ('data', (lambda value: value, 'data')),
    ('latitude', (float, 'iridium_latitude')),
    ('longitude', (float, 'iridium_longitude')),
    ('accuracy', (int, 'iridium_cep')),
])
DATABASE_PATH = 'data.db'
app = Flask('IridiumServer')
socketIO = SocketIO(app, cors_allowed_origins='*')
auth = HTTPTokenAuth(scheme='Bearer')
logger = logging.getLogger('IridiumServer')


@auth.verify_token
def verify_token(token):
    """
    Verify the bearer token from an incoming request.

    :param token: The token from the request.
    :return: True, if the token is valid.
    """
    if not request.is_secure:
        if app.debug:
            logger.debug(f'Allowing insecure access to "{request.url}", '
                         f'because debugging mode is active')
        else:
            logger.info(f'Denying access to "{request.url}", because no https was used')
            return
    if token == app.config['secret']:
        return True
    logger.info(f'Denying access to "{request.url}", because the token was invalid: "{token}"')


def getDatabase():
    """
    :return: The message database.
    """
    database = getattr(g, '_database', None)
    if database is None:
        database = g._database = sqlite3.connect(DATABASE_PATH)
    return database


@app.teardown_appcontext
def closeConnection(*_args):
    """
    Cleanup after a connection is closed.

    :param _args: ignored.
    """
    database = getattr(g, '_database', None)
    if database is not None:
        database.close()


@app.get('/')
@auth.login_required
def getAllMessages():
    """
    Provide a json list of all messages in the database.
    See the DATABASE_SCHEMA for a list of data points included for each message.

    :return: A list of messages.
    """
    with closing(getDatabase().execute('SELECT * FROM messages')) as query:
        messages = [{name: value for name, value
                     in zip(DATABASE_SCHEMA.keys(), message) if name != 'id'}
                    for message in query.fetchall()]
        return jsonify(messages)


@app.post('/new')
def newMessage():
    """
    Store a new message form the RockBlock device into the database and alert connected listeners.

    :return: Whether adding the message was successful.
    """
    requestData = request.get_json()
    if requestData is None:
        return jsonify(success=False, message='No data provided'), 400
    if requestData.get('imei') != app.config['RockBlockImei']:
        jsonify(success=False, message=f'Invalid imei'), 400
    data = OrderedDict()
    for databaseName, (converter, name) in DATABASE_SCHEMA.items():
        try:
            data[databaseName] = converter(requestData.get(name))
        except (ValueError, TypeError) as error:
            return jsonify(success=False, message=f'Invalid {name}: {error}'), 400
    database = getDatabase()
    try:
        database.execute(f'REPLACE INTO messages({", ".join(data)}) VALUES '
                         f'({", ".join("?" for _ in data)})', list(data.values()))
        database.commit()
    except sqlite3.Error as error:
        return jsonify(
            success=False, message=f'Error inserting the message into the database: {error}'), 500
    socketIO.send(data)
    return jsonify(success=True)


@socketIO.on('connect')
def onSocketIoConnected():
    """ Authenticate a new socketIO connection. """

    @auth.login_required()
    def check():
        return app.make_response(("OK", 200))

    if check().status_code != 200:
        raise ConnectionRefusedError()


def main():
    """
    Run the Iridium message server.

    :return: An exit code.
    """
    parser = ArgumentParser(
        description='A middleman server for incoming Iridium messages for the ASTER project.')
    parser.add_argument('-t', '--token', required=True,
                        help='The authentication token that is required for accessing messages')
    parser.add_argument('-i', '--imei', required=True,
                        help='The Imei for the RockBlock device for which to accept messages')
    parser.add_argument('-p', '--port', type=int, default=443,
                        help='The port on which the server should accept connections')
    parser.add_argument('--host', default='0.0.0.0',
                        help='The host name of the machine the server is running on')
    parser.add_argument('-d', '--debug', default=False, action='store_true',
                        help='Enable debug capabilities and logging')
    parser.add_argument('--sslPrivateKeyPath',
                        help='The path to the private key file used for the ssl context')
    parser.add_argument('--sslCertPath',
                        help='The path to the certificate file used for the ssl context')
    arguments = parser.parse_args()
    if arguments.sslCertPath is None or arguments.sslPrivateKeyPath is None:
        if not arguments.debug:
            logger.critical('Missing required ssl configuration')
            return 1
        sslArguments = {}
    else:
        # noinspection SpellCheckingInspection
        sslArguments = {
            'keyfile': arguments.sslPrivateKeyPath,
            'certfile': arguments.sslCertPath,
        }
    logging.addLevelName(logging.WARNING, 'WARN')
    logging.addLevelName(logging.CRITICAL, 'FATAL')
    # noinspection SpellCheckingInspection
    logging.basicConfig(format='[%(levelname)-5s] %(name)-20s %(asctime)-15s: %(message)s',
                        level=logging.DEBUG if arguments.debug else logging.INFO)
    app.config['secret'] = arguments.token
    app.config['RockBlockImei'] = arguments.imei
    with sqlite3.connect('data.db') as database:
        #  Must be kept in sync with the DATABASE_SCHEMA.
        database.execute('CREATE TABLE IF NOT EXISTS messages ('
                         'id INTEGER PRIMARY KEY, '
                         'time INTEGER NOT NULL, '
                         'data BLOB NOT NULL, '
                         'latitude DECIMAL NOT NULL, '
                         'longitude DECIMAL NOT NULL, '
                         'accuracy INTEGER NOT NULL)')
    socketIO.run(app, debug=arguments.debug, port=arguments.port,
                 host=arguments.host, **sslArguments)


if __name__ == '__main__':
    sys.exit(main())
