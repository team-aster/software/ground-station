# Iridium Message Server

This server acts as an intermediate message store for the messages that are sent
via the Iridium RockBlock module. It receives and stores them from the RockBlock service and
provides access as well as a SocketIO endpoint to notify listeners when a new message arrives.

### Authorization
The data on the server is protected by a shared secret, which is given as a command line argument
to the server on startup. All endpoints which allow retrieving data require the shared secret to be
provided.

## REST interface
```http request
GET /
Authorization: Bearer __secret__
```

Retrieve all stored Iridium messages currently in the database.
The shared secret needs to be provided in the `Authorization` header.
The data is encoded in hexadecimal. The time is a Unix timestamp, the accuracy
is in kilometers.

Example response:

```json
[
  {
    "time": 1636806399,
    "data": "cad2921814b83e26de280057ec000000003598c213000000000000000000000000006626e5fff6ff160006010100000000",
    "latitude": 67.8953,
    "longitude": 20.3331,
    "accuracy": 6
  }
]
```

## SocketIO endpoint
To connect to the SocketIO endpoint, the shared secret needs to be provided in the
`Authorization` header in the initial connection request.
When the server receives a new message from the Iridium network,
an event is posted to all connected SocketIO listeners with the
same contents that can be retrieved via the REST `GET` request.

Example event:
```json
{
  "time": 1636806399,
  "data": "cad2921814b83e26de280057ec000000003598c213000000000000000000000000006626e5fff6ff160006010100000000",
  "latitude": 67.8953,
  "longitude": 20.3331,
  "accuracy": 6
}
```
