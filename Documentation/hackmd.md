# Rough Ground Station Documentation

###### Task Tracking: https://trello.com/b/diMpbrPf/aster-ground-station

###### Project Name: Tarfala

### Required components:
- Docker
- Python 3.8+
- 
### Overview of the ASTER ground station concept
![Ground Station Concept](https://i.imgur.com/n0iNmHH.png)

## Currently included services:
- Docker
- InfluxDB (2.0 or higher)
- Python 3.6 or higher
- Grafana

## Docker

If using linux the file names need to be renamed to use `\` instead of `/`

The docker flag in docker-files/docker needs to be set to true prior to creating the image as it otherwise will not import the required package


##### `Docker_start` checks the available docker containers. It has a series of checks to ensure that all three required containers are running, and if not it either runs a up or start command depending on the nature of the problem


## InfluxDB Documentation

Configuring InfluxDB:
- With docker you need to either console into the container itself using:  `docker exec -it ground-station_influxdb_1 /bin/bash` when the container is running, or go to localhost:8086 when the container is running to use the AI
- **Organisation:** Aster
- **User Name:** admin (if created before 19.11.21; Noel Janes)
- **Password:** aster2019
- **Bucket:** aster


#### Bucket Layout

There will be two buckets initially:
- A testing bucket
- A mission bucket

We use the point structre for the datapoints as it is the easiest to implement into the python code. It is contained in AsterInflux.write
    
    
### RXSM Data Layout

When writing to the database each measurement is given a time stamp which corresponds to the current datatime 

An overview of the data layout used in the Aster ground station can be seen below (see https://docs.influxdata.com/influxdb/v2.0/reference/key-concepts/data-elements for information on the influx data elements):
| Measurement | Tag Key | Tag | Field Key | Field |
 | ----------- | ----------- |  ----------- | ----------- | ----------- | 
| Processor Time     | Telemetry Type       | See list in telemetry.csv | CPU Time | TM Time (ms) |
| FFU Temperatures     | Temperature Type       | Processor <br> Sensor | Processor <br> Number (1-6) | Temperature value (°C)
| Motor Readings   |   Motor Axis     | Roll <br> Pitch <br> Yaw | Current <br> <br> Speed | Current drawn by motor (A) <br> Motor RPM |
| Axis Readings | Axis | Roll <br> Pitch <br> Yaw | Acceleration <br> Secondary Acceleration <br> Rotation Speed <br> Secondary Rotation Speed <br> Magnetic Force <br> Secondary Magnetic Force | Acceleration on axis (m/s^2) <br> Rotation around the axis (rad/s) <br> Magnetic reading (mT) from primary and secondary IMU |
| System States | Time | Processor Timestamp | Current State | TM system state |
| Attitude | Type | X <br> Y <br> Z <br> W | Quaternion | Quaternion from the secondary IMU
| Voltage Readings | Voltage | Battery Level | Battery Level | Voltage of the FFU battery
| Controller Set Points | Axis | Axis | Set Points | The target motor speeds outputed by the adcs
| Message | Time | Processor Time | Message Type | Message Text [^1](/aarA6bQ3SFO0QImEW2A9tA) |
| Response | Telemetry Type | Response <br> Acknowledge | Command Number | Respone Value <br>if acknowledge: Command Number |
Pressure | Type | Pressure | Altimeter | Pressure Value

###### [^1](/aarA6bQ3SFO0QImEW2A9tA) Message Size is discarded

### Iridium Data Layout
#### TBD

## Grafana
If prompted: username: admin, password: aster

#### Configuring Variables in Grafana:


### Sending Telecommands and selecting ComPorts:

When selecting a telecommand or a comport you must make sure all the appropriate variables are correctly selected, and the confirmation variable has been set to Submit.
For comports the appropriate variables are:
- ComPort

For telecommands the appropriate variables are:
- Telecommand
- Argument
- Code
- Enable

Once you have selected it and have clicked submit then change then click the time range setting button and click apply time range (janky as anything, but welcome to Rexus). Once you have set what you want to transmit set the confirm variable to Waiting! This is very important to avoid sending the same command twice by accident

##### The grafana container takes the absolute longest to initialise, so I probably need to launch it later on in the program after som other tasks have completed

## Python

In order to use Influx2.0 rather than pre-1.7 the python package must be changed from `influxdb` to `influxdb_client`

###### Information on this package can be found here: https://github.com/influxdata/influxdb-client-python
https://docs.influxdata.com/influxdb/v2.0/api-guide/client-libraries/python/
Using the `.from_config_file()` function doesn't appear to work properly currently, unsure why. For now we will define everything ourselves and potentially read out of a config file at startup and use variables instead


### Python Socket Communication
The socket communication functions are provided in the class asterSocket (found in asterConnHandler.py)
The asterSocket provides the capability of communicating using sockets between the dockerised and local Pythons
#### Current Status:
- Init works
- Connect works
- Server Start works
- Transmit_test works (so just need to adapt transmit to match)
- Transmit works
- Receive works
- Pipeline seems to work
elements for information on the influx data elements):
| Measurement | Tag Key | Tag | Field Key | Field |
 | ----------- | ----------- |  ----------- | ----------- | ----------- | 
| Processor Time     | Telemetry Type       | See list in telemetry.csv | CPU Time | TM Time (ms) |
| FFU Temperatures     | Temperature Type       | Processor <br> Sensor | Processor <br> Number (1-6) | Temperature value (°C)
| Motor Readings   |   Motor Axis     | Pitch <br> Yaw <br> Roll | Current <br> <br> Speed | Current drawn by motor (A) <br> Motor RPM |
| Axis Readings | Axis | Pitch <br> Yaw <br> Roll | Acceleration <br> Rotation Speed <br> Magnetic Force | Acceleration on axis (m/s^2) <br> Rotation around the axis (rad/s) <br> Magnetic reading (mT) |
| System States | Time | Processor Timestamp | Current State | TM system state |
| Attitude | Type | Quaternion | Quaternion | Quaternion string (this is temporary until converter is completed)
| Message | Time | Processor Time | Message Type | Message Text [^1](/aarA6bQ3SFO0QImEW2A9tA) |
| Response | Telemetry Type | Response <br> Acknowledge | Command Number | Respone Value <br>if acknowledge: Command Number |
Pressure | Type | Pressure | Altimeter | Pressure Value