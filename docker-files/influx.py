""" This file handles importing of received messages from the experiment into the Influx database. """
import json
import os
import sys
import base64

from datetime import datetime, timezone, timedelta
from enum import Enum
from time import sleep
from typing import Dict, Any, Optional
from urllib.error import HTTPError

import requests

from influxdb_client import WritePrecision, InfluxDBClient, Point, WriteApi
from influxdb_client.client.exceptions import InfluxDBError

try:
    from ecom.message import Telemetry
except ImportError:  # Try the out of Docker locations
    try:
        sys.path.append(os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docker-files', 'communication', 'ECom')))

        from ecom.message import Telemetry
    except ImportError:
        raise ImportError('Shared communication database module missing, '
                          'run "git submodule update --init"')

from ecom.parser import TelemetryParser, ParserError
from ecom.database import CommunicationDatabase

from util import EComValueJsonEncoder


class AsterInflux:
    """ Allows interaction with the Influx database. """

    _TAG_TYPES = (Enum, bool)
    """ Types whose values should be put in tags instead of fields. """

    def __init__(self, config, parser: TelemetryParser, database: CommunicationDatabase):
        self._influxParams = config['influx2']
        self._host = self._influxParams['url']
        self._org = self._influxParams['org']
        self._token = self._influxParams['token']
        self._bucketName = self._influxParams['bucket']
        self._client = None
        self._lastReceivedOnChipTime = None
        self._lastChipReset = None

        self._parser = parser
        self._database = database
        self._RESPONSE_TYPE = database.getTelemetryByName('RESPONSE').id
        self._ACKNOWLEDGE_TYPE = database.getTelemetryByName('ACKNOWLEDGE').id

    def connect(self):
        """ Connects to the client and the database that is specified in the Influx Header. """
        print('Connecting to Influx Client: ', self._host)
        self._client = InfluxDBClient(url=self._host, token=self._token, org=self._org)
        while True:
            try:
                self._waitForServer()
                bucketApi = self._client.buckets_api()
                self._ensureBucket(bucketApi)
                break
            except InfluxDBError as error:
                print(f'[ERROR] Failed to connect to Influx: {error}')

    def saveTelemetryFrom(self, asterConnection):
        """
        Waits for telemetry from the ASTER connection and writes it to the database.
        This function blocks forever unless interrupted.

        :param asterConnection: The connection to the ASTER project.
        """
        while True:
            buffer = asterConnection.read()
            if buffer:
                with self._client.write_api(error_callback=self._onInfluxWriteFailed) as influx:
                    for telemetry in self._parser.parse(
                            buffer, errorHandler=lambda error: self._onTelemetryParserError(influx, error),
                            ignoredBytesHandler=lambda currentBuffer, ignoredBytes: self._onTelemetryIgnored(
                                influx, currentBuffer, ignoredBytes)):
                        self._handleTelemetry(telemetry, influx, asterConnection)

    def _handleTelemetry(self, telemetry, influx, asterConnection):
        """
        Writes the telemetry to the Influx database.

        :param telemetry: The telemetry to write to Influx.
        :param influx: The connection to the Influx database.
        :param asterConnection: The connection to the Aster project.
        """
        if telemetry.type is self._RESPONSE_TYPE or telemetry.type is self._ACKNOWLEDGE_TYPE:
            asterConnection.notifyTelecommandResponse(
                telemetry, isAcknowledgement=telemetry.type is self._ACKNOWLEDGE_TYPE)
        for name, onChipTime, utcTime, tags, fields in self._iterateTelemetryData(telemetry):
            if onChipTime is not None:
                if self._lastReceivedOnChipTime is None or onChipTime + 1000 < self._lastReceivedOnChipTime:
                    self._lastChipReset = datetime.now(timezone.utc) - timedelta(milliseconds=onChipTime)
                    utcTime = self._telemetryTimeToUtc(onChipTime)
                    self._write(influx, measurement='PROCESSOR_RESET', time=utcTime)
                    asterConnection.notifyResetDetected()
                self._lastReceivedOnChipTime = onChipTime
                # Write the current processor timestamp to Influx
                self._write(influx, measurement='PROCESSOR_TIME', time=utcTime, fields={'system time': onChipTime})
                if 'system time' not in fields:
                    fields['system time'] = onChipTime
            if not fields:
                # Influx does not accept records without a field. Convert a tag into a field as a workaround.
                try:
                    key, value = tags.popitem()
                    fields[key] = value
                except KeyError:
                    pass
            self._write(influx, measurement=name, time=utcTime, tags=tags, fields=fields)

    def _write(self, influx: WriteApi, measurement: str, time: Optional[datetime] = None,
               tags: Optional[Dict[str, Any]] = None, fields: Optional[Dict[str, Any]] = None):
        """
        Create a new measurement record at the given time with the tags and fields and write it to the database.

        :param influx: The connection to the Influx database.
        :param measurement: The name of the measurement to insert the datapoint into.
        :param time: The time of the record.
        :param tags: Tags of the record.
        :param fields: Fields of the record.
        """
        if time is None:
            time = datetime.now(timezone.utc)
        dataPoint = Point(measurement).time(time, WritePrecision.MS)
        if tags is not None:
            for tag, value in tags.items():
                dataPoint.tag(tag, value)
        if fields is not None:
            for field, value in fields.items():
                dataPoint.field(field, value)
        try:
            influx.write(self._bucketName, self._org, record=dataPoint)
        except (InfluxDBError, HTTPError) as error:
            print(f'Failed to write to Influx: {error}')

    def _iterateTelemetryData(self, telemetry: Telemetry):
        """
        Helper to iterate over all data points of the given telemetry, including nested data points.
        This will yield individual measurement records for arrays with children with their own time.

        :param telemetry: The telemetry to iterate on.
        :return: An iterator yielding the name, on-chip time, utc time, and value of each data point.
        """
        telemetryData = telemetry.data.copy()
        if telemetry.type is self._RESPONSE_TYPE:
            value = telemetryData.pop('value', None)
            if value is not None:
                telemetryData['value'] = json.dumps(value, cls=EComValueJsonEncoder)
        records = [(telemetry.type.name, datetime.now(timezone.utc), telemetryData)]
        while records:
            tags = {}
            fields = {}
            recordName, recordTime, recordData = records.pop(0)
            onChipTime = recordData.pop('time', None)
            recordTime = recordTime if onChipTime is None else self._telemetryTimeToUtc(onChipTime)
            dataItems = [(None, recordData)]
            while dataItems:
                parentName, items = dataItems.pop(0)
                for childName, child in items.items():
                    childName = childName if parentName is None else parentName + '.' + childName
                    if isinstance(child, dict):
                        if 'time' in child:
                            records.append((childName, recordTime, child))
                        else:
                            dataItems.append((childName, child))
                        continue
                    elif isinstance(child, list):
                        nonDictChildren = []
                        for childValue in child:
                            if isinstance(childValue, dict):
                                records.append((childName, recordTime, childValue))
                            else:
                                nonDictChildren.append(childValue)
                        if not nonDictChildren and not child:
                            continue
                        child = str(nonDictChildren)
                    if isinstance(child, bytes):
                        child = child.decode('utf-8', errors='replace')
                    if isinstance(child, self._TAG_TYPES):
                        tags[childName] = self._serialize(child)
                    elif isinstance(child, (str, int, float, bool, Enum)):
                        fields[childName] = self._serialize(child)
            yield recordName, onChipTime, recordTime, tags, fields

    @staticmethod
    def _serialize(value: Any) -> Any:
        """
        Serialize a value, so it can be written to the Influx database.

        :param value: The value to serialize.
        :return: The serialized value suitable for Influx.
        """
        if isinstance(value, Enum):
            return value.name
        return value

    def _telemetryTimeToUtc(self, time: Optional[int]) -> datetime:
        """
        Convert a telemetry time into UTC.

        :param time: The time from the telemetry
        :return: The UTC time corresponding to the telemetry time.
        """
        if time is None or self._lastChipReset is None:
            return datetime.now(timezone.utc)
        return self._lastChipReset + timedelta(milliseconds=time)

    def _waitForServer(self, retries: int = 10, waitingTime: int = 1):
        """
        Wait for the server to come online for `waitingTime`, `retries` times.
        Exit the program if the connection wasn't successfully established.

        :param retries: The amount of times to retry waiting for the Influx database.
        :param waitingTime: The time to wait between the first and second try, will double afterwards.
        """
        for i in range(retries):
            try:
                requests.get(self._host)
                return
            except requests.exceptions.ConnectionError:
                print(f'Waiting for Influx at {self._host}')
                sleep(waitingTime)
                waitingTime *= 2
        print(f'Failed to connect to Influx at {self._host}, exiting')
        sys.exit(1)

    def _ensureBucket(self, bucketApi):
        """
        Make sure the bucket exists and create it if it doesn't exist.

        :param bucketApi: The bucket API connected to the Influx database.
        """
        self._lsBuckets = bucketApi.find_bucket_by_name(self._bucketName)
        if self._lsBuckets is None:
            print('Creating bucket:', self._bucketName)
            bucketApi.create_bucket(bucket_name=self._bucketName)
        else:
            print('Bucket: {} exists'.format(self._bucketName))

    @staticmethod
    def _onInfluxWriteFailed(_conf, _data, exception):
        """
        Handle a failed write to the Influx database.

        :param _conf: Unused.
        :param _data: Unused.
        :param exception: The exception describing the failure.
        """
        print(f'[Error] Failed to write to Influx: {exception}')

    def _onTelemetryParserError(self, influx: WriteApi, parserError: ParserError):
        """
        Handle a telemetry parser error.

        :param influx: The connection to the Influx database.
        :param parserError: The parser error.
        """
        print(f'[Error] Failed to parse telemetry: {parserError}')
        self._write(influx, 'PARSER_ERRORS', tags={'type': parserError.__class__.__name__}, fields={
            'message': str(parserError),
            'buffer': base64.encodebytes(parserError.buffer).decode('utf-8'),
        })

    def _onTelemetryIgnored(self, influx, currentBuffer, ignoredBytes):
        """
        Handle ignored bytes.

        :param influx: The connection to the Influx database.
        :param currentBuffer: The current parser buffer including the ignored bytes.
        :param ignoredBytes: The number of ignored bytes.
        """
        print(f'[Warn ] Ignoring {ignoredBytes} bytes')
        self._write(influx, 'PARSER_ERRORS', tags={'type': 'IgnoreBytes'}, fields={
            'message': str(ignoredBytes),
            'buffer': base64.encodebytes(currentBuffer).decode('utf-8'),
        })
