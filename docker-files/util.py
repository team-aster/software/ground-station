import json
from enum import Enum


class EComValueJsonEncoder(json.JSONEncoder):
    """ A json encoder that allows writing ECom values. """
    def default(self, x):
        if isinstance(x, bytes):
            return x.decode('utf-8')
        if isinstance(x, Enum):
            return x.name
        return super().default(x)
