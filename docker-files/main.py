#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is the entry point of the docker groundstation.
Imports telemetry from the ASTER experiment, processes it and pushes the resulting data to appropriate influx databases.
Additionally, provides a REST server for Grafana to send telecommands.
"""

import os
import sys
import socket
import configparser

from time import sleep
from typing import Callable

from select import select
from threading import Thread
from contextlib import closing

from influx import AsterInflux
from grafana import runGrafanaInterfaceServer

try:
    from ecom.database import CommunicationDatabase, CommunicationDatabaseError

    DEFAULT_DATA_DIR = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'communicationDatabase'))
except ImportError:  # Try the out of Docker locations
    try:
        sys.path.append(os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docker-files', 'communication', 'ECom')))

        from ecom.database import CommunicationDatabase, CommunicationDatabaseError

        DEFAULT_DATA_DIR = os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docker-files', 'communication'))
    except ImportError:
        raise ImportError('Shared communication database module missing, '
                          'run "git submodule update --init"')

from ecom.message import Telemetry
from ecom.checksum import ChecksumVerifier
from ecom.response import ResponseTelemetryParser


class AsterConnection:
    """ A connection to the ASTER experiment. """

    def __init__(self, address, port):
        """
        Create a new connection, but don't connect yet.

        :param address: The IP address to connect to.
        :param port: The port to connect to.
        """
        super().__init__()
        self._address = (address, port)
        self._socket = None
        self._resetListeners = []
        self._telecommandResponseListeners = []

    @property
    def isConnected(self):
        """
        :return: Whether the connection is established.
        """
        currentSocket = self._socket
        if currentSocket is None:
            return False
        if getattr(currentSocket, '_closed'):
            self._socket = None
            return False
        return True

    def connect(self):
        """
        Attempt to establish a connection with the ASTER experiment.

        :return: A closable reference to the opened ASTER connection.
        """
        currentSocket = self._socket = socket.create_connection(self._address)
        currentSocket.setblocking(False)
        return closing(self)

    def close(self):
        """ Close the connection """
        currentSocket = self._socket
        if currentSocket is not None:
            currentSocket.close()
            self._socket = None

    def send(self, *args, **kwargs):
        """
        Send some data via the connection.

        :param args: Arguments for socket.send.
        :param kwargs: Keyword arguments for socket.send.
        """
        currentSocket = self._socket
        if currentSocket is None:
            raise socket.error('The connection is not established')
        try:
            currentSocket.sendall(*args, **kwargs)
        except socket.error:
            self.close()
            raise

    def read(self) -> bytes:
        """
        Receive bytes from the connection. This function will block until something is received.

        :return: The received bytes.
        """
        currentSocket = self._socket
        if currentSocket is None:
            raise socket.error('The connection is not established')
        while True:
            readyToRead, _, _ = select([currentSocket], [], [], 60)
            if readyToRead:
                receiveBuffer = currentSocket.recv(8192)
                if receiveBuffer:
                    return receiveBuffer
                self.close()
                raise socket.error('The connection was closed')

    def addOnResetListener(self, listener: Callable[[], None]):
        """
        Add a listener that will get called when a reset of the Aster experiment is detected.

        :param listener: The listener that will get called.
        """
        self._resetListeners.append(listener)

    def notifyResetDetected(self):
        """ Notify all listeners that a reset of the Aster experiment has been detected. """
        print('[INFO] Chip reset detected')
        for listener in self._resetListeners:
            listener()

    def addTelecommandResponseListener(self, listener: Callable[[Telemetry, bool], None]):
        """
        Add a listener that will get called when a response/acknowledgment
        of a telecommand is received from the Aster experiment.

        :param listener: The listener that will get called with the received response,
                         and whether it is an acknowledgment or not.
        """
        self._telecommandResponseListeners.append(listener)

    def notifyTelecommandResponse(self, response: Telemetry, isAcknowledgement: bool):
        """
        Notify all listeners that a response to a telecommand was received from the Aster experiment.

        :param response: The received response.
        :param isAcknowledgement: Whether the response is an acknowledgement.
        """
        for listener in self._telecommandResponseListeners:
            listener(response, isAcknowledgement=isAcknowledgement)


class AsterDockerPython:
    """ The main entrypoint of the ASTER docker groundstation. """

    def __init__(self):
        """ Initialize this groundstation instance and load the communication database and config. """
        # Load Telemetry and Telecommand information
        try:
            self._database = CommunicationDatabase(DEFAULT_DATA_DIR)
        except CommunicationDatabaseError as error:
            print(f'[ERROR] Failed to load the communication database: {error}')
            sys.exit(1)
        print('Loaded Database:', DEFAULT_DATA_DIR)
        self._parser = ResponseTelemetryParser(self._database, verifier=ChecksumVerifier(self._database))

        config = configparser.ConfigParser()
        configFilePath = os.path.join(os.path.dirname(__file__), 'config.ini')
        if not config.read(configFilePath):
            print(f'[ERROR] Unable to read config from {configFilePath}')
            sys.exit(1)
        self._influx = AsterInflux(config, self._parser, self._database)
        self._connection = AsterConnection(config['aster']['url'], int(config['aster']['port']))
        self._running = True

    def main(self):
        """ Run the groundstation forever until interrupted. """
        self._influx.connect()
        grafanaServerThread = Thread(target=runGrafanaInterfaceServer,
                                     args=(self._connection, self._parser, self._database))
        grafanaServerThread.start()
        while self._running:
            print('Connecting to ASTER...')
            try:
                with self._connection.connect() as asterConnection:
                    print('Connection to ASTER established')
                    try:
                        self._influx.saveTelemetryFrom(asterConnection)
                    except KeyboardInterrupt:
                        self._running = False
                    except socket.error as error:
                        print(f'Connection to ASTER was closed: {error}')
                    except Exception as error:
                        print(f'[Critical] Influx writer failed, restarting: {error}')
                        try:
                            self._connection.close()
                        except IOError:
                            pass
            except socket.error as error:
                print(f'Failed to connect to ASTER, retrying in 5 seconds: {error}')
                sleep(5)
        grafanaServerThread.join()


if __name__ == '__main__':
    try:
        instance = AsterDockerPython()
        instance.main()
    except KeyboardInterrupt:
        pass
