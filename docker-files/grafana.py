"""
A http REST server that allows the Grafana dashboard to query some information about the experiment connection
and to send telecommands.
"""

import os
import sys
import json
import socket
from time import sleep
from threading import Thread
from dataclasses import dataclass

from enum import Enum
from http import HTTPStatus
from typing import Optional, Any, Type

from flask import Flask, request
from flask_cors import CORS

from util import EComValueJsonEncoder

try:
    from ecom.database import CommunicationDatabase, Unit

    DEFAULT_DATA_DIR = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'communicationDatabase'))
except ImportError:  # Try the out of Docker locations
    try:
        sys.path.append(os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docker-files', 'communication', 'ECom')))

        from ecom.database import CommunicationDatabase, Unit

        DEFAULT_DATA_DIR = os.path.abspath(os.path.join(
            os.path.dirname(__file__), '..', 'docker-files', 'communication'))
    except ImportError:
        raise ImportError('Shared communication database module missing, '
                          'run "git submodule update --init"')

from ecom.message import iterateRequiredDatapoints, Telemetry
from ecom.checksum import ChecksumVerifier
from ecom.datatypes import loadTypedValue, ArrayType, DynamicSizeError, StructType
from ecom.serializer import TelecommandSerializer, DependantTelecommandDatapointType


def serializeTypedValue(value: Any, typ: Type) -> str:
    """
    Serialize a value with the given type.

    :param value: A value.
    :param typ: The type of the parsed value.
    :return: The serialized value.
    """
    if not isinstance(value, (typ, str)):
        if isinstance(value, bytes) and issubclass(typ, ArrayType):
            childType = typ.getElementTypeInfo().type
            if issubclass(childType, bytes):
                try:
                    if len(value) > len(typ):
                        raise ValueError(f'Value for bytes list is too large: {len(value)} (max is {len(typ)})')
                except DynamicSizeError:
                    pass
                return value.decode('utf-8')
        raise ValueError(f'Invalid value for {typ.__name__} type: {value!r} ({type(value)})')
    if issubclass(typ, bool):
        if not isinstance(value, str):
            return 'true' if value else 'false'
        if value not in ['true', 'false']:
            raise ValueError(f'Invalid value for bool type: {value!r} ({type(value)})')
        return value
    if issubclass(typ, bytes):
        if isinstance(value, bytes):
            return value.decode('utf-8')
        return value
    if issubclass(typ, StructType):
        if not isinstance(value, dict):
            raise ValueError(f'Invalid value for struct type: {value!r} ({type(value)})')
        return json.dumps(value, cls=EComValueJsonEncoder)
    if issubclass(typ, ArrayType):
        if not isinstance(value, list):
            raise ValueError(f'Invalid value for array type: {value!r} ({type(value)})')
        return json.dumps(value, cls=EComValueJsonEncoder)
    if isinstance(value, Enum):
        return value.name
    return json.dumps(value, cls=EComValueJsonEncoder)


def serializeType(typeInfo):
    """
    Convert the type information into a json representation.

    :param typeInfo: The type information to convert.
    :return: A json representation of the type information.
    """
    serialization = {}
    typeName = typeInfo.type.__name__
    if isinstance(typeInfo, Unit):
        serialization['unit'] = typeInfo.name
    if issubclass(typeInfo.type, Enum):
        typeName = 'enum'
        serialization['values'] = [{'name': value.name, 'description': value.__doc__}
                                   for value in typeInfo.type]
    serialization['name'] = typeName
    return serialization


def serializeDependantTelecommand(datapoint: DependantTelecommandDatapointType):
    """
    Serialize a dependant telecommand datapoint, so it can be sent to Grafana.

    :param datapoint: The dependant datapoint to serialize.
    :return: The serialized version of the dependant datapoint.
    """
    if issubclass(datapoint.provider.type.type, Enum):
        values = [value for value in datapoint.provider.type.type]
    elif issubclass(datapoint.provider.type.type, bool):
        values = [True, False]
    else:
        return None
    return {
        'dependsOn': datapoint.provider.name,
        'types': {
            value.name if isinstance(value, Enum) else value: serializeType(datapoint.configureWith(value).type)
            for value in values
        }
    }


def buildTelecommandsData(database):
    """
    Generate a representation of the telecommands that can be sent to Grafana.

    :param database: The communication database containing the telecommands.
    :return: A representation of all possible telecommands.
    """
    telecommands = {}
    for telecommand in database.telecommandTypes:
        command = telecommand.id.name
        telecommands[command] = {
            'description': telecommand.description,
            'arguments': {
                datapoint.name: {
                    'type': serializeDependantTelecommand(datapoint)
                    if isinstance(datapoint, DependantTelecommandDatapointType) else serializeType(datapoint.type),
                    'description': datapoint.description,
                    'default': None if datapoint.default is None or isinstance(
                        datapoint, DependantTelecommandDatapointType)
                    else serializeTypedValue(datapoint.default, datapoint.type.type),
                } for datapoint in iterateRequiredDatapoints(telecommand)
            },
        }
    return telecommands


@dataclass
class ConfigurationData:
    """ Represents the data known about a configuration item. """
    type: dict  # Information about the value type of the configuration.
    doc: Optional[str] = None  # A description of the configuration item.
    default: Optional[str] = None  # The default serialized value of the configuration item.
    value: Optional[str] = None  # The current serialized value of the configuration item or None, if unknown.
    updateId: Optional[int] = None  # The id of the command that was sent to update the value.


def buildServer(asterConnection, telecommandSerializer: TelecommandSerializer, database: CommunicationDatabase):
    """
    Build the Flask server app for the Grafana REST server.

    :param asterConnection: The connection to the ASTER experiment.
    :param telecommandSerializer: A serializer for telecommands.
    :param database: The communication database describing the communication
                     between the experiment and the groundstation.
    :return: The initialized Flask server app.
    """
    telecommandData = buildTelecommandsData(database)
    lastTelecommandId = None
    lastTelecommandError = None
    configuration = {configuration.name: ConfigurationData(
        serializeType(configuration.type), configuration.description, None if configuration.defaultValue is None else
        serializeTypedValue(configuration.defaultValue, configuration.type.type))
        for configuration in database.configurations}
    pendingConfigurationChanges = {}
    GET_CONFIG_TELECOMMAND_TYPE = database.getTelecommandByName('GET_CONFIG')
    SET_CONFIG_TELECOMMAND_TYPE = database.getTelecommandByName('SET_CONFIG')
    GET_FLIGHT_TELECOMMAND_TYPE = database.getTelecommandByName('GET_FLIGHT')
    SET_FLIGHT_TELECOMMAND_TYPE = database.getTelecommandByName('SET_FLIGHT')
    configResetThread = None
    configRequestMissingThread = None

    def requestAllConfig(onlyMissing=False):
        """
        Re-request all configuration values from the experiment.

        :param onlyMissing: Whether to only request missing config values.
        """
        for _configuration in database.configurations:
            if onlyMissing and configuration[_configuration.name].value is not None:
                continue
            commandId = telecommandSerializer.nextTelecommandCounter
            try:
                asterConnection.send(
                    telecommandSerializer.serialize(GET_CONFIG_TELECOMMAND_TYPE, config=_configuration.id))
            except socket.error as error:
                print(f'[WARN] Failed to request configuration {_configuration.name}: {error}')
            else:
                configuration[_configuration.id.name].updateId = commandId
            sleep(0.5)
        nonlocal configResetThread, configRequestMissingThread
        if onlyMissing:
            configRequestMissingThread = None
        else:
            configResetThread = None

    def resetConfiguration():
        """ Reset and asynchronously re-request all configuration values from the experiment. """
        nonlocal configResetThread
        if configResetThread is not None:
            return
        for _config in configuration.values():
            _config.value = None
        configResetThread = Thread(target=requestAllConfig, daemon=True)
        configResetThread.start()

    def requestMissingConfiguration():
        """ Asynchronously re-request all missing configuration values from the experiment. """
        nonlocal configRequestMissingThread
        if configRequestMissingThread is not None:
            return
        configRequestMissingThread = Thread(target=requestAllConfig, kwargs={'onlyMissing': True}, daemon=True)
        configRequestMissingThread.start()

    def handleTelecommandResponse(telemetry: Telemetry, isAcknowledgement: bool):
        updateCommandNumber = telemetry.data['command number']
        respondedCommandId = telemetry.data['command'].id
        if isAcknowledgement:
            if respondedCommandId is not SET_CONFIG_TELECOMMAND_TYPE.id \
                    and respondedCommandId is not SET_FLIGHT_TELECOMMAND_TYPE.id:
                return
            value = pendingConfigurationChanges.pop(updateCommandNumber, None)
            if value is None:
                return
        else:
            if respondedCommandId is not GET_CONFIG_TELECOMMAND_TYPE.id and \
                    respondedCommandId is not GET_FLIGHT_TELECOMMAND_TYPE.id:
                return
            value = telemetry.data['value']
        for name, _config in configuration.items():
            if _config.updateId == updateCommandNumber:
                _config.updateId = None
                for configInfo in database.configurations:
                    if configInfo.name == name:
                        if isinstance(value, float):
                            value = round(value, 6)
                        elif isinstance(value, bytes):
                            value = value.rstrip(b'\0')
                        _config.value = serializeTypedValue(value, configInfo.type.type)

    app = Flask(__name__)
    CORS(app, supports_credentials=True)
    asterConnection.addOnResetListener(requestMissingConfiguration)
    asterConnection.addTelecommandResponseListener(handleTelecommandResponse)

    def failRest(description, code=HTTPStatus.BAD_REQUEST, setTelecommandError=False):
        """
        Generate a failure response for a REST request.

        :param description: The description of the failure.
        :param code: The HTTP code of the failure.
        :param setTelecommandError: Whether to set the last telecommand error.
        :return: A response representing the failure.
        """
        print(f'Failing request: {description}')
        if setTelecommandError:
            nonlocal lastTelecommandError
            lastTelecommandError = description
        return {'status': 'error', 'reason': description}, code

    def succeedRest(**kwargs):
        """
        Generate a success response for a REST request.

        :param kwargs: Additional data to add to the response.
        :return: A response representing the success.
        """
        return {'status': 'ok', **kwargs}

    @app.get('/')
    def testConnection():
        """
        An endpoint to check if the REST server is online.

        :return: Always HTTP OK.
        """
        return HTTPStatus.OK.phrase, HTTPStatus.OK.value

    @app.get('/telecommands')
    def telecommands():
        """
        :return: A JSON description of all available telecommands.
        """
        return telecommandData

    @app.get('/isConnected')
    def isConnected():
        """
        :return: Whether the groundstation is connected to the experiment.
        """
        return succeedRest(connected=asterConnection.isConnected)

    @app.get('/lastCommandId')
    def lastCommandId():
        """
        :return: The id of the last sent telecommand.
        """
        return {'command': lastTelecommandId}

    @app.get('/lastCommandError')
    def lastCommandError():
        """
        :return: The error message of the last telecommand, if any.
        """
        return {'error': lastTelecommandError}

    @app.get('/config')
    def config():
        """
        :return: The current configuration.
        """
        return succeedRest(config=configuration)

    @app.post('/updateConfigs')
    def updateConfigs():
        """
        Update the values of all configurations from the experiment.

        :return: A REST response indicating whether updating was successful or not.
        """
        resetConfiguration()
        return succeedRest()

    @app.post('/command')
    def sendTelecommand():
        """
        Send a telecommand.
        The telecommand is passed via the "command" query argument.
        Optional arguments to the telecommand are passed via the "arguments" query argument
        as a json encoded key value mapping.

        :return: A response indication if the telecommand was sent or not.
        """
        nonlocal lastTelecommandId, lastTelecommandError
        lastTelecommandError = None
        command = str(request.args.get('command'))
        requestArguments = request.args.get('arguments')
        if not requestArguments:
            requestArguments = {}
        else:
            try:
                requestArguments = json.loads(requestArguments)
            except (json.JSONDecodeError, TypeError) as error:
                return failRest(f'Failed to parse arguments: {error}', setTelecommandError=True)
        if not isinstance(requestArguments, dict):
            return failRest(f'Invalid value passed as arguments: {requestArguments!r}', setTelecommandError=True)
        try:
            telecommand = database.getTelecommandByName(command)
        except ValueError:
            return failRest(f'Invalid command "{command}"', setTelecommandError=True)
        arguments = {}
        for argument in iterateRequiredDatapoints(telecommand):
            try:
                value = requestArguments[argument.name]
            except KeyError:
                if argument.default is None:
                    return failRest(f'Missing argument "{argument.name}"', setTelecommandError=True)
                value = argument.default
            if isinstance(argument, DependantTelecommandDatapointType):
                argument = argument.configureWith(arguments[argument.provider.name])
            try:
                value = loadTypedValue(value, argument.type.type)
            except (ValueError, TypeError) as error:
                return failRest(f'Invalid value for argument {argument.name!r} ({value!r}): {error}',
                                setTelecommandError=True)
            arguments[argument.name] = value
        print(f'Sending Telecommand: {telecommand}({arguments})')
        lastTelecommandId = telecommandSerializer.nextTelecommandCounter
        if telecommand.id is GET_CONFIG_TELECOMMAND_TYPE.id:
            _config = configuration[arguments['config'].name]
            _config.value = None
            _config.updateId = lastTelecommandId
        elif telecommand.id is SET_CONFIG_TELECOMMAND_TYPE.id:
            _config = configuration[arguments['config'].name]
            pendingConfigurationChanges[lastTelecommandId] = arguments.get('value')
            _config.updateId = lastTelecommandId
        elif telecommand.id is GET_FLIGHT_TELECOMMAND_TYPE.id:
            _config = configuration['flight mode']
            _config.value = None
            _config.updateId = lastTelecommandId
        elif telecommand.id is SET_FLIGHT_TELECOMMAND_TYPE.id:
            _config = configuration['flight mode']
            pendingConfigurationChanges[lastTelecommandId] = arguments.get('enable')
            _config.updateId = lastTelecommandId
        try:
            asterConnection.send(telecommandSerializer.serialize(telecommand, **arguments))
            return succeedRest()
        except (socket.error, TypeError, ValueError) as error:
            print(f'Failed to send telecommand: {error}')
            return failRest(f'Send failed: {error}', code=HTTPStatus.BAD_GATEWAY, setTelecommandError=True)

    return app


def runGrafanaInterfaceServer(asterConnection, telecommandSerializer, database):
    """
    Run the REST server for the Grafana dashboard on port 2019.
    This function blocks until the server exits.

    :param asterConnection: The connection to the ASTER experiment.
    :param telecommandSerializer: A serializer for telecommands.
    :param database: A database describing the communication between the groundstation and the experiment.
    """
    server = buildServer(asterConnection, telecommandSerializer, database)
    server.run(host='0.0.0.0', port=2019)


def main():
    """ Run the REST server with a fake mock connection. """
    mockConnection = type(
        'MockConnection', (), {
            'send': lambda _, data: print(f'Sending: {data!r}'),
            'addOnResetListener': lambda *_: None,
            'addTelecommandResponseListener': lambda *_: None,
        })()
    database = CommunicationDatabase(DEFAULT_DATA_DIR)
    runGrafanaInterfaceServer(mockConnection, TelecommandSerializer(
        database, verifier=ChecksumVerifier(database)), database)


if __name__ == '__main__':
    main()
